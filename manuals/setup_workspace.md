Установка рабочего окружения catkin
====================================

## Создаём каталог с исходниками проекта

```
mkdir -p ~/gazebo/mower/catkin_ws/src
```

## Переходим в каталог с исходниками

```
cd ~/gazebo/mower/catkin_ws/src
```

## Инициируем рабочее пространство catkin

```
catkin_init_workspace
```

Получаем вывод команды

```
Creating symlink "/home/ignat/gazebo/mower/catkin_ws/src/CMakeLists.txt" pointing to "/opt/ros/noetic/share/catkin/cmake/toplevel.cmake"
```

## Переходим в каталог рабочего пространства catkin

```
cd  ~/gazebo/mower/catkin_ws
```

## Запускаем сборку рабочего пространства catkin

```
catkin_make
```


