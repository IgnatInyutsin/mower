Документация
===================================================

## Создание пустого проекта

- [Установка ПО](setup_software.md)
- [Создание рабочего пространства catkin](setup_workspace.md)
- [Создание пакетов](setup_package.md)
- [Создание мира для моделирования](setup_world.md)
- [Создание модели робота](setup_model.md)
- [Запуск моделирования в gazebo](startup_modeling.md)
- [Просмотр готовой urfd-модели](show_urdf.md)
- [Запуск моделирования в rviz](start_rviz_modeling.mg)


## Детали, соединения, размеры

- [dxf-файл](dxf/common_model.dxf)
- [pdf-версия](pdf/common_model.pdf)
- [растровое изображение](png/common_model.png)

![Изображение модели робота](png/common_model.png)


## Ссылки на статьи

- [Robotic simulation scenarios with Gazebo and ROS](https://blog.generationrobots.com/en/robotic-simulation-scenarios-with-gazebo-and-ros/)
- [Список стандартных материалов gazebo](https://wiki.ros.org/simulator_gazebo/Tutorials/ListOfMaterials)
- [Таблица RGB цветов](https://www.rapidtables.com/web/color/RGB_Color.html)
- [Пример простой URDF модели с двумя сочленениями](http://gazebosim.org/tutorials/?tut=ros_urdf)
- [Описание атрибутов соединения(joint)](http://wiki.ros.org/urdf/XML/joint)
- [О соотношении скорости вращения и усилия](http://wiki.ros.org/pr2_controller_manager/safety_limits)
