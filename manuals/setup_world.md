Создание мира для моделирования
=====================================

## Переходим в каталог пакета с инструкциями для запуска модели в gazebo

```
cd ~/gazebo/mower/catkin_ws/src/mower_gazebo
```

## Создаём каталоги c описанием скрита запуска и мира моделирования

```
mkdir launch worlds
```

## Создаём описание мира моделирования

Создаём в каталоге worlds файл *mower.world* следующего содержания

```
<?xml version="1.0"?>
<sdf version="1.4">
<!-- Описание мира для моделирования -->
<world name="myworld">

    <!-- Подключаем модель источника освещения "Солнце" -->
    <include>
	<uri>model://sun</uri>
    </include>

    <!-- Подключаем модель земной поверхности -->
    <include>
	<uri>model://ground_plane</uri>
    </include>

</world>
</sdf>
```

## Создаём описание процесса запуска моделирования

Создаём в каталоге launch файл *mower_world.launch* следующего содержания

```
<launch>      
    <!-- Запустить для моделирования пустой мир -->    
    <include file="$(find gazebo_ros)/launch/empty_world.launch">           

        <!-- Передать в пустой мир объекты нашего мира для моделирования -->
        <arg name="world_name" value="$(find mower_gazebo)/worlds/mower.world"/>     

        <!-- Моделирование проводить с отображением в графическом интерфейсе -->
        <arg name="gui" value="true"/>

    </include>

    <!-- 
        С помощью скрипта обработки xacro-макросов(параметр command) обработать описание файлов модели 
        робота(mower.xacro) и сформировать один XML-текст для моделирования (его содержимое будет помещено в 
        переменную robot_description) 
    -->
    <param name="robot_description" command="$(find xacro)/xacro '$(find mower_description)/urdf/mower.xacro'" />

    <!-- 
        Запустиь моделирование робота mower как узел ROS и передать ему XML-описание 
        через переменную robot_description 
    -->
    <node name="gazonbot_spawn" pkg="gazebo_ros" type="spawn_model" output="screen"
    args="-urdf -param robot_description -model mower" />

</launch>
```
