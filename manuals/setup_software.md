Установка программного обеспечения.
====================================
Debian-овые


Добавляем файл с репозиторием
sudo sh -c 'echo "deb [trusted=yes] http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros-latest.list'

sudo apt-get update
sudo apt install ros-noetic-desktop-full



ArchLinux-овые


Установка идет через AUR, прочтите документации по нему перед какими-либо действиями
git clone https://aur.archlinux.org/ros-noetic-desktop-full.git
cd ros-noetic-desktop-full
makepkg -si