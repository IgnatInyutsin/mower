Запуск моделирования в rviz
=============================

# Запуск roscore

```
roscore
```

Вывод будет примерно такой

```
... logging to /home/ignat/.ros/log/6ec05e22-2048-11eb-98f1-f44d30a3360a/roslaunch-desktop2.local-8080.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://desktop2.local:36175/
ros_comm version 1.15.8


SUMMARY
========

PARAMETERS
 * /rosdistro: noetic
 * /rosversion: 1.15.8

NODES

auto-starting new master
process[master]: started with pid [8090]
ROS_MASTER_URI=http://desktop2.local:11311/

setting /run_id to 6ec05e22-2048-11eb-98f1-f44d30a3360a
process[rosout-1]: started with pid [8101]
started core service [/rosout]
```

Обработка команды не прекратится и прерывать её не надо

# Создание пакета rviz

Перейдите в каталог *src*

```
cd ~/gazebo/mower/catkin_ws/src
```

Создайте пакет rviz


```
catkin_create_pkg mower_rviz
```

```
Created file mower_rviz/package.xml
Created file mower_rviz/CMakeLists.txt
Successfully created files in /home/ignat/gazebo/mower/catkin_ws/src/mower_rviz. Please adjust the values in package.xml.
```

Создайте в нём каталоги launch и rviz

```
mkdir ~/gazebo/mower/catkin_ws/src/mower_rviz/launch
```

```
mkdir ~/gazebo/mower/catkin_ws/src/mower_rviz/rviz
```

# Запуск rviz

```
rosrun rviz rviz
```

# Сохранение пустого конфига rviz

Файл->Сохранить конфиг как

Сохраните конфиг rviz как *~/gazebo/mower/catkin_ws/src/mower_rviz/rviz/mower.rviz*

# Создание описания процесса запуска rviz

Создаёте в файле *~/gazebo/mower/catkin_ws/src/mower_rviz/launch/mower.launch* следующег содержания

```
<launch>

  <arg name="gui" default="true" />
  <arg name="rvizconfig" default="$(find mower_rviz)/rviz/mower.rviz" />

  <param name="robot_description" command="$(find xacro)/xacro --inorder '$(find mower_description)/urdf/mower.xacro'" />
  <param name="use_gui" value="$(arg gui)"/>

  <node if="$(arg gui)" name="joint_state_publisher" pkg="joint_state_publisher_gui" type="joint_state_publisher_gui" />
  <node unless="$(arg gui)" name="joint_state_publisher" pkg="joint_state_publisher" type="joint_state_publisher" />
  <node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher" />
  <node name="rviz" pkg="rviz" type="rviz" args="-d $(arg rvizconfig)" required="true" />

</launch>
```

# Создание запускаемого файла

```
touch ~/gazebo/mower/catkin_ws/src/start_rviz_modeling.bash
```

```
chmod +x ~/gazebo/mower/catkin_ws/src/start_rviz_modeling.bash
```

Со следующим содержимым

```
#!/bin/bash

# Запуск моделирования робота в rviz
roslaunch mower_rviz mower.launch

```

## Запуск моделирования

```
bash ~/gazebo/mower/catkin_ws/src/start_rviz_modeling.bash
```

# Добавление добота в моделирование

Add->Robot Model


