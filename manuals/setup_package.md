Создание пакетов.
=======================

## Переходим в каталог с исходниками

```
cd ~/gazebo/mower/catkin_ws/src
```

## Создаём пакет с инструкциями для запуска модели в gazebo

У этого пакета в зависимостях прописан *gazebo_ros*

```
catkin_create_pkg mower_gazebo gazebo_ros
```

Получаем вывод команды

```
Created file mower_gazebo/package.xml
Created file mower_gazebo/CMakeLists.txt
Successfully created files in /home/ignat/gazebo/mower/catkin_ws/src/mower_gazebo. Please adjust the values in package.xml.
```

## Создаём пакет с описанием структуры робота

```
catkin_create_pkg mower_description
```

Получаем вывод команды

```
Created file mower_description/package.xml
Created file mower_description/CMakeLists.txt
Successfully created files in /home/ignat/gazebo/mower/catkin_ws/src/mower_description. Please adjust the values in package.xml.
```

## Создаём пакет с описанием управления роботом

```
catkin_create_pkg mover_control
```

Получаем вывод команды

```
Created file mover_control/package.xml
Created file mover_control/CMakeLists.txt
Successfully created files in /home/ignat/gazebo/mower/catkin_ws/src/mover_control. Please adjust the values in package.xml.
```

## Создаём мир для моделирования

В каталоге **(пакет c инструкциями для запуска модели в gazebo)

```
mower_gazebo
```