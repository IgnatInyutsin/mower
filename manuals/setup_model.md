Создание модели робота
===========================


## Перейдём в каталог описания робота

```
cd ~/gazebo/mower/catkin_ws/src/mower_description
```

## Создадим там каталог для urdf-модели

```
mkdir urdf
```

## Создадим там файл *mower.xacro* следующего содержания

```
<?xml version="1.0"?>
<!-- Описание модели робота-->

<robot name="gazonbot" xmlns:xacro="http://www.ros.org/wiki/xacro">

</robot>
```
