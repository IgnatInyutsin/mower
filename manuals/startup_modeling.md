Запуск моделирования
==========================

## Перейдём в каталог с исходниками

```
cd ~/gazebo/mower/catkin_ws/src
```

## Создадим там скрипт запуска моделирования

```
touch start_modeling.bash
```

## Сделаем этот файл исполняемым

```
chmod +x start_modeling.bash
```

## Разместим там следующее содержимое

```
#!/bin/bash

# Запуск моделирования робота
roslaunch mower_gazebo mower_world.launch
```

## Запустим этот скрипт

```
bash start_modeling.bash
```

или просто

```
./start_modeling.bash
```

или просто кликом в файловом менеджере, например *mc*
